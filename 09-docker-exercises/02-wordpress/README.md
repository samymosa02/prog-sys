# 09 - Docker Exercises

## 02 - Wordpress

### Start the containers

```bash
docker-compose up -d
```

### Check service availability
```bash
docker ps
# or docker-compose ps
```

### Visit http://localhost:8080 to install Wordpress

![Wordpress Admin](./screenshots/wp_admin.png)
![Wordpress Install](./screenshots/wp_install.png)
![Wordpress Installed](./screenshots/wp_installed.png)
![Wordpress Site](./screenshots/wp_site.png)

### Stop and remove the containers

```bash
docker-compose down
```