# 09 - Docker Exercises

## 01 - DHCP

### Pull image
  
```bash
docker pull networkboot/dhcpd
```

### Add an IP address

```bash
# See the config file at ./dhcpd.conf

sudo ip addr add 10.16.64.1/24 dev eth0
```

### Run the container

```bash
docker run --name dhcpd -d --net=host -v "$(pwd)/dhcpd.conf:/data/dhcpd.conf" networkboot/dhcpd eth0
```

### Checks

```bash
# Check service availability
docker ps

# Check the logs
docker logs dhcpd

# Check DHCP is working
sudo dhclient -i -v eth0
```

### Stop and remove the container
```bash
# Stop the container
docker stop dhcpd

# Remove the container
docker rm dhcpd
```