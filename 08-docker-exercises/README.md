# 08 - Docker Exercises

## 01 - Jenkins

### Start Jenkins using the official Jenkins image
```bash
# Pull the image
docker pull jenkins/jenkins

# Run the container
docker run -d -p 8080:8080 -p 50000:50000 --name jenkins jenkins/jenkins

# Get the initial admin password
docker exec jenkins cat /var/jenkins_home/secrets/initialAdminPassword
```

### Check service availability
```bash
docker ps
```

### Visit http://localhost:8080 and enter the initial admin password
![Jenkins Login](./screenshots/jenkins-login.png)

### Install suggested plugins
![Jenkins Plugins](./screenshots/jenkins-plugins.png)

### Finish the setup and start using Jenkins
![Jenkins Dashboard](./screenshots/jenkins-dashboard.png)

### Stop and remove the container
```bash
# Stop the container
docker stop jenkins

# Remove the container
docker rm jenkins
```

## 02 - Jenkins from scratch

### Prerequisites
Download the Jenkins war file from [here](https://jenkins.io/download/) and place it in this directory. Make sure the file is named `jenkins.war`.

### Build and run Jenkins from scratch

```bash
# Build the image (see Dockerfile for details)
docker build -t jenkins-from-scratch .

# Run the container
docker run -d -p 8080:8080 --name jenkins-from-scratch jenkins-from-scratch

# Get the initial admin password
docker exec jenkins-from-scratch cat /var/jenkins_home/secrets/initialAdminPassword
```

### Visit http://localhost:8080/jenkins and enter the initial admin password
![Jenkins Login](./screenshots/jenkins-login.png)

### Install suggested plugins
![Jenkins Plugins](./screenshots/jenkins-plugins.png)

### Finish the setup and start using Jenkins
![Jenkins Dashboard](./screenshots/jenkins-dashboard.png)

### Stop and remove the container
```bash
# Stop the container
docker stop jenkins-from-scratch

# Remove the container
docker rm jenkins-from-scratch

```
