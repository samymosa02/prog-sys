#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <time.h>

#define ARRAY_SIZE 100000000
#define _MIN(a, b) ((a) < (b) ? (a) : (b))
#define _MAX(a, b) ((a) > (b) ? (a) : (b))

typedef struct
{
  int min;
  int max;
} min_max_t;

typedef struct
{
  int *array;
  int length;
} array_slice_t;

// global variable
min_max_t min_max_res = {0, 0};
pthread_mutex_t min_max_res_mutex = PTHREAD_MUTEX_INITIALIZER;

int *create_random_array(int size)
{
  int *array = malloc(sizeof(int) * size);
  if (array == NULL)
    return NULL;

  srand(time(NULL));
  for (int i = 0; i < size; i++)
    array[i] = rand() % 1000;

  return array;
}

void *compute_min_max(void *arg)
{
  array_slice_t *args = (array_slice_t *)arg;

  int min = args->array[0];
  int max = args->array[0];

  // compute min and max for the slice
  for (int i = 0; i < args->length; i++)
  {
    min = _MIN(min, args->array[i]);
    max = _MAX(max, args->array[i]);
  }

  // update global min and max
  pthread_mutex_lock(&min_max_res_mutex);
  min_max_res.min = _MIN(min, min_max_res.min);
  min_max_res.max = _MAX(max, min_max_res.max);
  pthread_mutex_unlock(&min_max_res_mutex);

  return &min_max_res;
}

int main(int argc, char **argv)
{
  if (argc < 2)
  {
    printf("Usage: %s <nb_threads>\n", argv[0]);
    return EXIT_FAILURE;
  }

  int nb_threads = atoi(argv[1]);
  if (nb_threads < 1)
  {
    dprintf(STDERR_FILENO, "Error: nb_threads must be >= 1\n");
    return EXIT_FAILURE;
  }

  int *array = create_random_array(ARRAY_SIZE);
  if (array == NULL)
  {
    dprintf(STDERR_FILENO, "Error: could not allocate array\n");
    return EXIT_FAILURE;
  }

  // timeit start
  struct timeval start, end;
  gettimeofday(&start, NULL);

  // init min_max_res
  min_max_res.min = array[0];
  min_max_res.max = array[0];

  // create threads
  pthread_t threads[nb_threads];
  for (int i = 0; i < nb_threads; i++)
  {
    array_slice_t args = {
        .array = array + i * (ARRAY_SIZE / nb_threads),
        .length = ARRAY_SIZE / nb_threads,
    };
    if (pthread_create(&threads[i], NULL, compute_min_max, &args) != 0)
    {
      dprintf(STDERR_FILENO, "Error: could not create thread #%d\n", i);
      return EXIT_FAILURE;
    }
  }

  // wait for threads to finish
  for (int i = 0; i < nb_threads; i++)
  {
    if (pthread_join(threads[i], NULL) != 0)
    {
      dprintf(STDERR_FILENO, "Error: could not join thread #%d\n", i);
      return EXIT_FAILURE;
    }
  }

  // timeit end
  gettimeofday(&end, NULL);
  long elapsed = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec);
  printf("nb_threads=%d, min=%d, max=%d, took %ld microseconds\n", nb_threads, min_max_res.min, min_max_res.max, elapsed);

  free(array);
}

/* QUESTIONS
 * 
 * Proposez une version threadée de ce programme. Le résultat sera écrit dans une ou plusieurs variables
 * (une seule variable si une structure est utilisée). Testez ce programme avec 2, 4, 8 threads et en
 * augmentant la taille du tableau. Que constatez-vous ?
 *
 * On constate que le temps d'exécution diminue avec le nombre de threads, jusqu'à un certain point.
 * En effet, au delà d'un certain nombre de threads, qui dépend de la taille du tableau et de la machine,
 * le temps d'exécution augmente à nouveau.
 */