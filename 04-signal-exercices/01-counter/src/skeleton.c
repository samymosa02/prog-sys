#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int sigint_count = 0;

void sigint_handler(int sig)
{
  printf("SIGINT received, count = %d\n", ++sigint_count);
}

void sigterm_handler(int sig)
{
  printf("SIGTERM received, exiting...\n");
  exit(0);
}

int main()
{
  struct sigaction sigint_action;
  sigint_action.sa_handler = sigint_handler;
  sigemptyset(&sigint_action.sa_mask);
  sigint_action.sa_flags = 0;
  sigaction(SIGINT, &sigint_action, NULL);

  struct sigaction sigterm_action;
  sigterm_action.sa_handler = sigterm_handler;
  sigemptyset(&sigterm_action.sa_mask);
  sigterm_action.sa_flags = 0;
  sigaction(SIGTERM, &sigterm_action, NULL);

  while (1)
    ;

  return 0;
}