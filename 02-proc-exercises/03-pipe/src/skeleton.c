#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>

#define READ_MODE 0
#define WRITE_MODE 1

int exec_pipe_helper(int pipefd[2], const char *file, char *const argv[], int mode)
{
  if (close(pipefd[!mode]) == -1)
  {
    perror("close");
    return EXIT_FAILURE;
  }

  if (dup2(pipefd[mode], mode) == -1)
  {
    perror("dup2");
    return EXIT_FAILURE;
  }

  if (close(pipefd[mode]) == -1)
  {
    perror("close");
    return EXIT_FAILURE;
  }

  execvp(file, argv);
  perror("execvp");
  return EXIT_FAILURE;
}

int exec_pipe_reader(int pipefd[2], const char *file, char *const argv[])
{
  return exec_pipe_helper(pipefd, file, argv, READ_MODE);
}

int exec_pipe_writer(int pipefd[2], const char *file, char *const argv[])
{
  return exec_pipe_helper(pipefd, file, argv, WRITE_MODE);
}

int exec_pipe(const char *file1, char *const argv1[], const char *file2, char *const argv2[])
{
  int reader_status, writer_status;
  int pipefd[2];
  if (pipe(pipefd) == -1)
  {
    perror("pipe");
    return EXIT_FAILURE;
  }

  pid_t writer_pid, reader_pid2;
  switch (writer_pid = fork())
  {
  case -1:
    perror("fork writer");
    return EXIT_FAILURE;
  case 0:
    return exec_pipe_writer(pipefd, file1, argv1);
  default:
    break;
  }

  switch (reader_pid2 = fork())
  {
  case -1:
    perror("fork reader");
    return EXIT_FAILURE;
  case 0:
    return exec_pipe_reader(pipefd, file2, argv2);
  default:
    break;
  }

  if (close(pipefd[READ_MODE]) == -1)
  {
    perror("close pipefd[READ_MODE]");
    return EXIT_FAILURE;
  }

  if (close(pipefd[WRITE_MODE]) == -1)
  {
    perror("close pipefd[WRITE_MODE]");
    return EXIT_FAILURE;
  }

  if (waitpid(writer_pid, &writer_status, 0) == -1)
  {
    perror("waitpid writer");
    return EXIT_FAILURE;
  }

  if (waitpid(reader_pid2, &reader_status, 0) == -1)
  {
    perror("waitpid reader");
    return EXIT_FAILURE;
  }

  return WEXITSTATUS(writer_status) ? WEXITSTATUS(writer_status) : WEXITSTATUS(reader_status);
}

int left_hand()
{
  int status;
  char *ps_argv[] = {"ps", "eaux", NULL};
  char *grep_argv[] = {"grep", "^root ", NULL};
  pid_t pid = fork();
  switch (pid)
  {
  case -1:
    perror("fork");
    return EXIT_FAILURE;

  case 0:
    if (close(STDOUT_FILENO) == -1)
    {
      perror("close STDOUT_FILENO");
      return EXIT_FAILURE;
    }

    if (open("/dev/null", O_WRONLY) == -1)
    {
      perror("open /dev/null");
      return EXIT_FAILURE;
    }
    exit(exec_pipe("ps", ps_argv, "grep", grep_argv));

  default:
    if (waitpid(pid, &status, 0) == -1)
    {
      perror("waitpid");
      return EXIT_FAILURE;
    }

    return WEXITSTATUS(status);
  }
}

int right_hand()
{
  if (write(STDOUT_FILENO, "root est connecté\n", 19) != 19)
  {
    perror("write STDOUT_FILENO");
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

int main(int argc, char **argv)
{
  int status = EXIT_SUCCESS;
  if ((status = left_hand()) != EXIT_SUCCESS)
    return status;

  return right_hand();
}
