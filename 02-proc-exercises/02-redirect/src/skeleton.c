#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char **argv)
{
  // Ensure there's at least one argument
  if (argc < 2)
  {
    printf("Usage: %s <message>\n", argv[0]);
    return EXIT_FAILURE;
  }

  pid_t pid = fork();
  switch (pid)
  {
  case -1:
    perror("fork");
    return EXIT_FAILURE;

  case 0:
    printf("Child: pid=%d\n", getpid());
    // close(1); // Will redirect stdout
    close(2); // Will redirect stderr

    char filename[] = "/tmp/proc-exercise-XXXXXX";
    int fd;
    if ((fd = mkstemp(filename)) == -1)
    {
      perror("mkstemp");
      return EXIT_FAILURE;
    }

    printf("File descriptor: %d\n\n", fd);
    execvp(argv[1], argv + 1);
    
    // If we get here, execv failed
    perror("execv");
    return EXIT_FAILURE;
    break;

  default:
    printf("Parent: pid=%d\n", getpid());
    int status;
    if (wait(&status) == -1)
    {
      perror("wait");
      return EXIT_FAILURE;
    }
    printf("Parent: child exited with code %d\n", WEXITSTATUS(status));
    break;
  }
}
