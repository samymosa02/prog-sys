#include<stdio.h>
#include<stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char** argv)
{
  // Simple fork
  // Child prints getpid() (child) and getppid() (parent)
  // Parent prints child pid, wait for child to finish and prints child's return code

  pid_t pid = fork();
  switch(pid) {
    case -1:
      perror("fork");
      return EXIT_FAILURE;
    case 0:
      printf("Child: pid=%d, ppid=%d\n", getpid(), getppid());
      return EXIT_SUCCESS;
    default:
      printf("Parent: child pid=%d\n", pid);
      int status;
      if (wait(&status) == -1) {
        perror("wait");
        return EXIT_FAILURE;
      }
      printf("Parent: child return code=%d\n", WEXITSTATUS(status));
      return EXIT_SUCCESS;
  }
}
