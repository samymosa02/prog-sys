#include "app.h"

void reverse_buffer(char *buffer, int size)
{
  for (int i = 0; i < size / 2; i++)
  {
    char tmp = buffer[i];
    buffer[i] = buffer[size - i - 1];
    buffer[size - i - 1] = tmp;
  }
}

int my_print_reverse(char *bin_input_param)
{
  int fd_in = open(bin_input_param, O_RDONLY);
  if (fd_in == -1)
  {
    perror("Could not open input file");
    return EXIT_FAILURE;
  }

  // Go to end of file
  char buffer[BUFFER_SIZE];
  int offset = lseek(fd_in, 0, SEEK_END);
  if (offset == -1)
  {
    perror("Could not seek to end of file");
    return EXIT_FAILURE;
  }

  int nb_bytes_to_read = BUFFER_SIZE;
  while (offset > 0)
  {
    // Compute offset and nb_bytes_to_read
    offset -= BUFFER_SIZE;
    if (offset < 0)
    {
      nb_bytes_to_read = BUFFER_SIZE + offset;
      offset = 0;
    }

    // Go backwards
    if (lseek(fd_in, offset, SEEK_SET) == -1)
    {
      perror("Could not seek to offset");
      return EXIT_FAILURE;
    }

    if (read(fd_in, buffer, nb_bytes_to_read) == -1)
    {
      perror("Could not read file");
      return EXIT_FAILURE;
    }

    reverse_buffer(buffer, nb_bytes_to_read);

    if (write(STDOUT_FILENO, buffer, nb_bytes_to_read) == -1)
    {
      perror("Could not write to stdout");
      return EXIT_FAILURE;
    }

    // Go back to previous position (before read)
    if (lseek(fd_in, offset, SEEK_SET) == -1)
    {
      perror("Could not seek back to offset");
      return EXIT_FAILURE;
    }
  }

  if (close(fd_in) == -1)
  {
    perror("Could not close input file");
    return EXIT_FAILURE;
  }

  printf("\n");
  return EXIT_SUCCESS;
}
