#ifndef _ARGPARSE_H
#define _ARGPARSE_H

#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_PATH_LENGTH 4096
#define USAGE_SYNTAX "[OPTIONS] -i INPUT"
#define USAGE_PARAMS "OPTIONS:\n\
  -i, --input  INPUT_FILE  : input file\n\
***\n\
  -h, --help    : display this help\n\
"

void print_usage(char *bin_name);
void print_params(char *input_file);
void parse_args(int argc, char **argv, char *input_file);
void check_required_args(char *input_file);

#endif // _ARGPARSE_H