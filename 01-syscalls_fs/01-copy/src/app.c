#include "app.h"

int my_copy(char *bin_input_param, char *bin_output_param)
{
  // Open files
  int fd_in = open(bin_input_param, O_RDONLY);
  if (fd_in == -1)
  {
    perror("Could not open input file");
    return EXIT_FAILURE;
  }

  int fd_out = open(bin_output_param, O_WRONLY | O_CREAT | O_TRUNC, 0644);
  if (fd_out == -1)
  {
    perror("Could not open output file");
    return EXIT_FAILURE;
  }

  // Copy input file to output file
  char buffer[BUFFER_SIZE];
  int nb_read = 0;
  while ((nb_read = read(fd_in, buffer, BUFFER_SIZE)) > 0)
  {
    if (write(fd_out, buffer, nb_read) == -1)
    {
      perror("Could not write to output file");
      return EXIT_FAILURE;
    }
  }

  if (nb_read == -1)
  {
    perror("Could not read from input file");
    return EXIT_FAILURE;
  }

  // Close files
  if (close(fd_in) == -1)
  {
    perror("Could not close input file");
    return EXIT_FAILURE;
  }

  if (close(fd_out) == -1)
  {
    perror("Could not close output file");
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
