/**
 * \file skeleton.c
 * \brief Basic parsing options skeleton.
 * \author Pierre L. <pierre1.leroy@orange.com>
 * \version 0.1
 * \date 10 septembre 2016
 *
 * Basic parsing options skeleton exemple c file.
 */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>

#include "argparse.h"
#include "app.h"

int main(int argc, char** argv)
{
  char input_file[MAX_PATH_LENGTH] = {0};
  char output_file[MAX_PATH_LENGTH] = {0};

  parse_args(argc, argv, input_file, output_file);
  check_required_args(input_file, output_file);
  print_params(input_file, output_file);

  return my_copy(input_file, output_file);
}
