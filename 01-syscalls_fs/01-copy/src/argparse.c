#include "argparse.h"

/**
 * Binary options declaration
 * (must end with {0,0,0,0})
 *
 * \see man 3 getopt_long or getopt
 * \see struct option definition
 */
static struct option options[] =
    {
        {"help", no_argument, 0, 'h'},
        {"input", required_argument, 0, 'i'},
        {"output", required_argument, 0, 'o'},
        {0, 0, 0, 0}};

/**
 * Binary options string
 * (linked to optionn declaration)
 *
 * \see man 3 getopt_long or getopt
 */
const char *optstr = "hi:o:";

void print_usage(char *bin_name)
{
  printf("USAGE: %s %s\n\n%s\n", bin_name, USAGE_SYNTAX, USAGE_PARAMS);
}

void print_params(char *input_file, char *output_file)
{
  printf("** PARAMS **\n%-8s: %s\n%-8s: %s\n",
         "input", input_file,
         "output", output_file);
}

void check_required_args(char *input_file, char *output_file)
{
  if (input_file[0] == '\0' || output_file[0] == '\0')
  {
    fprintf(stderr, "Bad usage! See HELP [--help|-h]\n");
    exit(EXIT_FAILURE);
  }
}

void parse_args(int argc, char **argv, char *input_file, char *output_file)
{
  int opt, optidx;

  while ((opt = getopt_long(argc, argv, optstr, options, &optidx)) != -1)
  {
    switch (opt)
    {
    case 'i':
      if (optarg)
        strncpy(input_file, optarg, MAX_PATH_LENGTH);
      break;
    case 'o':
      if (optarg)
        strncpy(output_file, optarg, MAX_PATH_LENGTH);
      break;
    case 'h':
      print_usage(argv[0]);
      exit(EXIT_SUCCESS);
    default:
      break;
    }
  }
}
