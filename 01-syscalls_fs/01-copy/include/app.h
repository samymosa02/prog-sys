#ifndef _APP_H
#define _APP_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFFER_SIZE 4096

int my_copy(char *bin_input_param, char *bin_output_param);

#endif // _APP_H