#ifndef _APP_H
#define _APP_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFFER_SIZE 4096

typedef struct
{
  int fd;
  char *buffer;
  char mode;
  int buffer_size;
  int buffer_pos;
} MY_FILE;

MY_FILE *my_open(char *pathname, char *mode);
int my_close(MY_FILE *file);
int my_getc(MY_FILE *file);
int my_putc(int c, MY_FILE *file);
int my_close(MY_FILE *file);
int my_flush(MY_FILE *file);

#endif // _APP_H