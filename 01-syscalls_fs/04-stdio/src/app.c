#include "app.h"

MY_FILE *my_open(char *pathname, char *mode)
{
  int fd = -1;
  switch (mode[0])
  {
  case 'r':
    fd = open(pathname, O_RDONLY);
    break;
  case 'w':
    fd = open(pathname, O_WRONLY | O_CREAT | O_TRUNC, 0666);
    break;
  default:
    break;
  }
  if (fd == -1)
    return NULL;

  char *buffer = malloc(BUFFER_SIZE);
  if (buffer == NULL)
    return NULL;

  MY_FILE *file = malloc(sizeof(MY_FILE));
  if (file == NULL)
    return NULL;

  file->fd = fd;
  file->mode = mode[0];
  file->buffer = buffer;
  file->buffer_size = BUFFER_SIZE;
  file->buffer_pos = file->mode == 'r' ? file->buffer_size : 0;

  return file;
}

int my_close(MY_FILE *file)
{
  int fd = file->fd;

  // Flush the buffer before closing the file.
  if (file->mode == 'w' && my_flush(file) == EOF)
    return EOF;

  free(file->buffer);
  free(file);
  return close(fd);
}

int my_getc(MY_FILE *file)
{
  if (file->buffer_pos == file->buffer_size)
  {
    int bytes_read = read(file->fd, file->buffer, file->buffer_size);
    if (bytes_read <= 0)
      return EOF;
    if (bytes_read < file->buffer_size)
      file->buffer[bytes_read] = EOF;
    file->buffer_pos = 0;
  }
  return file->buffer[file->buffer_pos++];
}

int my_putc(int c, MY_FILE *file)
{
  if (file->buffer_pos == file->buffer_size)
  {
    if (my_flush(file) == EOF)
      return EOF;
  }
  file->buffer[file->buffer_pos++] = c;
  return c;
}

int my_flush(MY_FILE *file)
{
  int bytes_written = write(file->fd, file->buffer, file->buffer_pos);
  if (bytes_written <= 0)
    return EOF;
  file->buffer_pos = 0;
  return 0;
}
