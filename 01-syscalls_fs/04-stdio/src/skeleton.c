/**
 * \file skeleton.c
 * \brief Basic parsing options skeleton.
 * \author Pierre L. <pierre1.leroy@orange.com>
 * \version 0.1
 * \date 10 septembre 2016
 *
 * Basic parsing options skeleton exemple c file.
 */
#include<stdio.h>
#include<stdlib.h>

#include "app.h"

int test_read()
{
  MY_FILE *file = my_open("test/test.txt", "r");
  if (file == NULL)
    return EXIT_FAILURE;

  int c;
  printf("Reading file: ");
  while ((c = my_getc(file)) != EOF)
    printf("%c", c);
  printf("\n");

  return my_close(file);
}

int test_write()
{
  MY_FILE *file = my_open("test/test.txt", "w");
  if (file == NULL)
    return EXIT_FAILURE;

  printf("Writing file...\n");
  for (int i = '!'; i <= '~'; i++)
    if(my_putc(i, file) == EOF)
      return EXIT_FAILURE;

  return my_close(file);
}

int test_copy()
{
  MY_FILE *file_in = my_open("Makefile", "r");
  if (file_in == NULL)
    return EXIT_FAILURE;

  MY_FILE *file_out = my_open("test/test_copy.txt", "w");
  if (file_out == NULL)
    return EXIT_FAILURE;

  int c;
  printf("Copying file...\n");
  while ((c = my_getc(file_in)) != EOF)
    if(my_putc(c, file_out) == EOF)
      return EXIT_FAILURE;

  return my_close(file_in) || my_close(file_out);
}

int main(int argc, char** argv)
{
  return test_write() || test_read() || test_copy();
}
