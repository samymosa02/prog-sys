#include "app.h"
#include "argparse.h"

char *get_file_permissions(char *filepath);
char *format_size(long size);
char *format_date(time_t timestamp);

int my_ls(char *dir_path)
{
    DIR *dir = opendir(dir_path);
    if (dir == NULL)
    {
        fprintf(stderr, "Error opening directory '%s': %s\n", dir_path, strerror(errno));
        return EXIT_FAILURE;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_name[0] != '-' && entry->d_name[0] != '/')
        {
            char filepath[MAX_PATH_LENGTH];
            snprintf(filepath, sizeof(filepath), "%s/%s", dir_path, entry->d_name);
            if (print_file_info(filepath) != EXIT_SUCCESS)
            {
                fprintf(stderr, "Error printing file info for '%s': %s\n", filepath, strerror(errno));
            }
        }
    }

    if (errno != 0)
    {
        fprintf(stderr, "Error reading directory entries for '%s': %s\n", dir_path, strerror(errno));
        closedir(dir);
        return EXIT_FAILURE;
    }

    if (closedir(dir) == -1)
    {
        fprintf(stderr, "Error closing directory '%s': %s\n", dir_path, strerror(errno));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int print_file_info(char *filepath)
{
    struct stat file_stat;
    if (stat(filepath, &file_stat) == -1)
    {
        fprintf(stderr, "Error getting file information: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    char *filename = basename(filepath);

    struct passwd *owner_info = getpwuid(file_stat.st_uid);
    char *owner_name = (owner_info != NULL) ? owner_info->pw_name : "Unknown";

    struct group *group_info = getgrgid(file_stat.st_gid);
    char *group_name = (group_info != NULL) ? group_info->gr_name : "Unknown";

    char *permissions = get_file_permissions(filepath);
    if (permissions == NULL)
    {
        fprintf(stderr, "Error getting file permissions for '%s': %s\n", filepath, strerror(errno));
        return EXIT_FAILURE;
    }

    char *date_str = format_date(file_stat.st_mtime);
    if (date_str == NULL)
    {
        fprintf(stderr, "Error formatting date for '%s': %s\n", filepath, strerror(errno));
        free(permissions);
        return EXIT_FAILURE;
    }

    printf("%-20s | %-12s | %s:%s | %-8ld | %s\n",
           filename, permissions, owner_name, group_name, file_stat.st_size, date_str);

    free(permissions);
    free(date_str);

    return EXIT_SUCCESS;
}

char *get_file_permissions(char *filepath)
{
    struct stat file_stat;
    if (stat(filepath, &file_stat) == -1)
    {
        fprintf(stderr, "Error getting file information for '%s': %s\n", filepath, strerror(errno));
        return NULL;
    }

    char *perms = malloc(11);
    if (perms == NULL)
    {
        fprintf(stderr, "Error allocating memory: %s\n", strerror(errno));
        return NULL;
    }

    perms[0] = (S_ISDIR(file_stat.st_mode)) ? 'd' : '-';
    perms[1] = (file_stat.st_mode & S_IRUSR) ? 'r' : '-';
    perms[2] = (file_stat.st_mode & S_IWUSR) ? 'w' : '-';
    perms[3] = (file_stat.st_mode & S_IXUSR) ? 'x' : '-';
    perms[4] = (file_stat.st_mode & S_IRGRP) ? 'r' : '-';
    perms[5] = (file_stat.st_mode & S_IWGRP) ? 'w' : '-';
    perms[6] = (file_stat.st_mode & S_IXGRP) ? 'x' : '-';
    perms[7] = (file_stat.st_mode & S_IROTH) ? 'r' : '-';
    perms[8] = (file_stat.st_mode & S_IWOTH) ? 'w' : '-';
    perms[9] = (file_stat.st_mode & S_IXOTH) ? 'x' : '-';
    perms[10] = '\0';

    return perms;
}

char *format_date(time_t timestamp)
{
    struct tm *time_info = localtime(&timestamp);
    if (time_info == NULL)
    {
        fprintf(stderr, "Error getting local time: %s\n", strerror(errno));
        return NULL;
    }

    char *date_str = malloc(20);
    if (date_str == NULL)
    {
        fprintf(stderr, "Error allocating memory for date: %s\n", strerror(errno));
        return NULL;
    }

    int result = snprintf(date_str, 20, "%02d%02d%02d @ %02dh%02d",
                          time_info->tm_mday, time_info->tm_mon + 1, time_info->tm_year % 100,
                          time_info->tm_hour, time_info->tm_min);

    if (result < 0 || result >= 20)
    {
        fprintf(stderr, "Error formatting date string\n");
        free(date_str);
        return NULL;
    }

    return date_str;
}