#ifndef _APP_H
#define _APP_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <libgen.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>

int my_ls(char *bin_input_param);
int print_file_info(char *filepath);

#endif // _APP_H